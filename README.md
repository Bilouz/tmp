# create your drupal and postgres config here, based off the last assignment

version: '3.1'

services:
  drupal:
    image: drupal:8.4.2-apache
    ports:
      - "8080:80"
    volumes:
      - /var/www/html/modules
      - /var/www/html/profiles
      - /var/www/html/sites
      - /var/www/html/themes      
  postgres:
    image: postgres:9.6
    environment: 
      - POSTGRES_PASSWORD=mypasswd